package com.mykolak

import com.mykolak.nycschool.api.SchoolApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/*
setUp() method is called before each test to create an instance of the SchoolApi interface using Retrofit.
testGetSchools() method tests the getSchools() method of the SchoolApi interface by making a network request
 and verifying that the returned list of School objects has the correct size and properties.
testGetSatSchoolsScores() method tests the getSatSchoolsScores() method of the SchoolApi interface
by making a network request and verifying that the returned list of SchoolSAT objects has the correct size and properties.

Will be fail at the end.
*/

class SchoolApiTest {

    private lateinit var schoolApi: SchoolApi

    @Before
    fun setUp() {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/resource/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        schoolApi = retrofit.create(SchoolApi::class.java)
    }

    @Test
    fun testGetSchools() {
        runBlocking {
            val schools = schoolApi.getSchools()

            assertEquals(440, schools.size)
            assertEquals("dbn000010", schools[0].dbn)
            assertEquals("HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES", schools[0].school_name)
            assertEquals("212-406-9411", schools[0].phone_number)
            assertEquals("www.henrystreet.org", schools[0].website)
            assertEquals("220 HENRY STREET", schools[0].primary_address_line_1)
            assertEquals("NEW YORK", schools[0].city)
            assertEquals("NY", schools[0].state_code)
            assertEquals("10002", schools[0].zip)
        }
    }

    @Test
    fun testGetSatSchoolsScores() {
        runBlocking {
            val satSchoolsScores = schoolApi.getSatSchoolsScores()

            assertEquals(478, satSchoolsScores.size)
            assertEquals("01M292", satSchoolsScores[0].dbn)
            assertEquals(404, satSchoolsScores[0].sat_math_avg_score)
            assertEquals(355, satSchoolsScores[0].sat_critical_reading_avg_score)
            assertEquals(363, satSchoolsScores[0].sat_writing_avg_score)
        }
    }
}
