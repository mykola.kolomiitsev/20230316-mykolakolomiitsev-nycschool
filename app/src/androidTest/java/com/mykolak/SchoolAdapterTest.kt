package com.mykolak

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mykolak.nycschool.R
import com.mykolak.nycschool.adapter.SchoolAdapter
import com.mykolak.nycschool.model.School
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SchoolAdapterTest {

    private lateinit var adapter: SchoolAdapter

    @Before
    fun setUp() {
        adapter = SchoolAdapter(listOf(
            School("dbn1", "School 1", "1234567890", "www.school1.com", "123 Main St", "New York", "NY", "10001", 85),
            School("dbn2", "School 2", "0987654321", null, "456 Broadway", "Brooklyn", "NY", "11211", 750)
        ))
    }

    @Test
    fun testGetItemCount() {
        assertEquals(2, adapter.itemCount)
    }

    @Test
    fun testGetItemViewType() {
        assertEquals(0, adapter.getItemViewType(0))
        assertEquals(0, adapter.getItemViewType(1))
    }

    @Test
    fun testCreateViewHolder() {
        val parent = LayoutInflater.from(ApplicationProvider.getApplicationContext())
            .inflate(R.layout.school_item, null)
        val viewHolder = adapter.onCreateViewHolder(parent as ViewGroup, 0)
        assertEquals(TextView::class.java, viewHolder.nameTextView.javaClass)
        assertEquals(TextView::class.java, viewHolder.addressTextView.javaClass)
        assertEquals(TextView::class.java, viewHolder.phoneNumberTextView.javaClass)
    }

    @Test
    fun testBindViewHolder() {
        val parent = LayoutInflater.from(ApplicationProvider.getApplicationContext())
            .inflate(R.layout.school_item, null)
        val viewHolder = adapter.onCreateViewHolder(parent as ViewGroup, 0)
        adapter.onBindViewHolder(viewHolder, 0)
        assertEquals("School 1", viewHolder.nameTextView.text.toString())
        assertEquals("123 Main St", viewHolder.addressTextView.text.toString())
        assertEquals("1234567890", viewHolder.phoneNumberTextView.text.toString())
    }
}