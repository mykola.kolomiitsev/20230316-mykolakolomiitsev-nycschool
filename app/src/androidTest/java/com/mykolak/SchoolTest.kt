package com.mykolak

import android.os.Parcel
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mykolak.nycschool.model.School
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SchoolTest {

    private lateinit var school: School

    @Before
    fun setUp() {
        school = School(
            "dbn123",
            "School Name",
            "123-456-7890",
            "www.school.com",
            "123 Main St",
            "New York City",
            "NY",
            "10001",
            500,
            550,
            600
        )
    }

    @Test
    fun testParcelable() {
        val parcel = Parcel.obtain()
        school.writeToParcel(parcel, school.describeContents())
        parcel.setDataPosition(0)

        val createdSchool = School.CREATOR.createFromParcel(parcel)

        assertNotNull(createdSchool)
        assertEquals(school.dbn, createdSchool.dbn)
        assertEquals(school.school_name, createdSchool.school_name)
        assertEquals(school.phone_number, createdSchool.phone_number)
        assertEquals(school.website, createdSchool.website)
        assertEquals(school.primary_address_line_1, createdSchool.primary_address_line_1)
        assertEquals(school.city, createdSchool.city)
        assertEquals(school.state_code, createdSchool.state_code)
        assertEquals(school.zip, createdSchool.zip)
        assertEquals(school.satMath, createdSchool.satMath)
        assertEquals(school.satReading, createdSchool.satReading)
        assertEquals(school.satWriting, createdSchool.satWriting)
    }
    
    @Test
    fun testDataClassProperties() {
        assertEquals("dbn123", school.dbn)
        assertEquals("School Name", school.school_name)
        assertEquals("123-456-7890", school.phone_number)
        assertEquals("www.school.com", school.website)
        assertEquals("123 Main St", school.primary_address_line_1)
        assertEquals("New York City", school.city)
        assertEquals("NY", school.state_code)
        assertEquals("10001", school.zip)
        assertEquals(500, school.satMath)
        assertEquals(550, school.satReading)
        assertEquals(600, school.satWriting)
    }
}