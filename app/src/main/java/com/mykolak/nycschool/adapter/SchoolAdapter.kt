package com.mykolak.nycschool.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mykolak.nycschool.R
import com.mykolak.nycschool.SchoolDetailsActivity
import com.mykolak.nycschool.model.School

class SchoolAdapter(private val schools: List<School>) : RecyclerView.Adapter<SchoolAdapter.ViewHolder>() {
        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val nameTextView: TextView = itemView.findViewById(R.id.nameTextView)
            val addressTextView: TextView = itemView.findViewById(R.id.addressTextView)
            val phoneNumberTextView: TextView = itemView.findViewById(R.id.phoneNumberTextView)
            val countOfPupils: TextView = itemView.findViewById(R.id.countOfPupils)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.school_item, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val school = schools[position]
            holder.nameTextView.text = school.school_name
            holder.addressTextView.text = school.primary_address_line_1
            holder.phoneNumberTextView.text = school.phone_number
            holder.countOfPupils.text = school.total_students.toString()
            holder.itemView.setOnClickListener {
                val intent = Intent(holder.itemView.context, SchoolDetailsActivity::class.java)
                intent.putExtra("school", school)
                holder.itemView.context.startActivity(intent)
            }
        }

        override fun getItemCount(): Int {
            return schools.size
        }
    }