package com.mykolak.nycschool

import android.os.Bundle
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mykolak.nycschool.adapter.SchoolAdapter
import com.mykolak.nycschool.api.NetworkUtils
import com.mykolak.nycschool.model.SchoolViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: SchoolViewModel
    private lateinit var adapter: SchoolAdapter
    private lateinit var swipeRefreshLayout : SwipeRefreshLayout
    private lateinit var schoolsRecyclerView : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        swipeRefreshLayout = findViewById<SwipeRefreshLayout>(R.id.swipe_refresh_layout)
        swipeRefreshLayout.setOnRefreshListener {
            // Your code to refresh the data goes here
            loadData()
        }

        schoolsRecyclerView = findViewById<RecyclerView>(R.id.schoolsRecyclerView)
        adapter = SchoolAdapter(emptyList())
        schoolsRecyclerView.adapter = adapter
        schoolsRecyclerView.layoutManager = LinearLayoutManager(this)
        loadData()
    }

    private fun loadData()
    {
        if (NetworkUtils.isInternetAvailable(this)) {
            viewModel = ViewModelProvider(this).get(SchoolViewModel::class.java)
            swipeRefreshLayout.isRefreshing = true
            viewModel.getSchools().observe(this) { schools ->
                adapter = SchoolAdapter(schools.sortedByDescending { it.total_students })
                schoolsRecyclerView.adapter = adapter
                swipeRefreshLayout.isRefreshing = false // Hide the progress bar
            }
        } else {
            swipeRefreshLayout.isRefreshing = false // Hide the progress bar
            Toast.makeText(this, "No Internet", Toast.LENGTH_LONG).show()
        }
    }
}