package com.mykolak.nycschool.model

import android.os.Parcel
import android.os.Parcelable

data class SchoolSAT(
    val dbn: String,
    var sat_math_avg_score: String,
    var sat_critical_reading_avg_score: String,
    var sat_writing_avg_score: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dbn)
        parcel.writeString(sat_math_avg_score)
        parcel.writeString(sat_critical_reading_avg_score)
        parcel.writeString(sat_writing_avg_score)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SchoolSAT> {
        override fun createFromParcel(parcel: Parcel): SchoolSAT {
            return SchoolSAT(parcel)
        }

        override fun newArray(size: Int): Array<SchoolSAT?> {
            return arrayOfNulls(size)
        }
    }
}