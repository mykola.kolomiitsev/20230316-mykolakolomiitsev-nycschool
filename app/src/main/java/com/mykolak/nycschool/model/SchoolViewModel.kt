package com.mykolak.nycschool.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mykolak.nycschool.api.SchoolRepository
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import java.io.IOException

class SchoolViewModel : ViewModel() {
        private val repository = SchoolRepository()

    fun getSchools(): LiveData<List<School>> {
        val schoolsLiveData = MutableLiveData<List<School>>()
        viewModelScope.launch {
            try {
                val schools = repository.getSchools()
                schoolsLiveData.value = schools
            } catch (e: IOException) {
            }
        }
        return schoolsLiveData
    }

    fun getSchoolSAT(): LiveData<List<SchoolSAT>> {
        val schoolsSATLiveData = MutableLiveData<List<SchoolSAT>>()
        viewModelScope.launch {
            try {
                val schoolSAT = repository.getSchoolsSat()
                schoolsSATLiveData.value = schoolSAT
            } catch (e: IOException) {
            }
        }
        return schoolsSATLiveData
    }
}