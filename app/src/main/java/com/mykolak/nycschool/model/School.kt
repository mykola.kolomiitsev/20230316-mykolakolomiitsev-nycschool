package com.mykolak.nycschool.model

import android.os.Parcel
import android.os.Parcelable

data class School(
    val dbn: String,
    val school_name: String,
    val phone_number: String,
    val website: String?,
    val primary_address_line_1: String?,
    val city: String?,
    val state_code: String?,
    val zip: String?,
    val total_students: Int,
    val latitude : String,
    val longitude : String
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!
        )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dbn)
        parcel.writeString(school_name)
        parcel.writeString(phone_number)
        parcel.writeString(website)
        parcel.writeString(primary_address_line_1)
        parcel.writeString(city)
        parcel.writeString(state_code)
        parcel.writeString(zip)
        parcel.writeInt(total_students)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<School> {
        override fun createFromParcel(parcel: Parcel): School {
            return School(parcel)
        }

        override fun newArray(size: Int): Array<School?> {
            return arrayOfNulls(size)
        }
    }
}