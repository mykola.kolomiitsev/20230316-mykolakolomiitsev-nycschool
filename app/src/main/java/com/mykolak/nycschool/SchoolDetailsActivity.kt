package com.mykolak.nycschool

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.mykolak.nycschool.api.NetworkUtils
import com.mykolak.nycschool.model.School
import com.mykolak.nycschool.model.SchoolSAT
import com.mykolak.nycschool.model.SchoolViewModel


class SchoolDetailsActivity : AppCompatActivity() {
    private lateinit var viewModel: SchoolViewModel
    lateinit var schoolData: SchoolSAT
    lateinit var schoolID: String
    lateinit var SATData: SchoolSAT
    lateinit var phone: TextView
    lateinit var geoposition: TextView
    lateinit var email: TextView
    lateinit var totalPupils: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_details)
        val actionBar: ActionBar? = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        title = "Details"
        phone = findViewById(R.id.phoneNumber)
        geoposition = findViewById(R.id.geoPosition)
        email = findViewById(R.id.email)
        totalPupils = findViewById(R.id.totalPupils)

        val data = intent.extras
        val school: School? = data!!.getParcelable<Parcelable>("school") as School?
        totalPupils.text = school!!.total_students.toString()
        phone.text = school!!.phone_number
        phone.setOnClickListener {
            val intent =
                Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", school!!.phone_number, null))
            startActivity(intent)
        }
        geoposition.text = school!!.primary_address_line_1 + ", " + school!!.city
        email.text = school!!.website
        email.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://" + school!!.website))
            startActivity(browserIntent)
        }
        geoposition.setOnClickListener {
            val gmmIntentUri =
                Uri.parse("google.navigation:q=" + school.latitude + "," + school.longitude)
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
        schoolID = school!!.dbn
        if (NetworkUtils.isInternetAvailable(this)) {
            viewModel = ViewModelProvider(this)[SchoolViewModel::class.java]
            viewModel.getSchoolSAT().observe(this) { school ->
                schoolData = getFilteredList(schoolID, school) as SchoolSAT
                findViewById<TextView>(R.id.mathScoreTextView).text =
                    "Math:\n${schoolData!!.sat_math_avg_score}"
                findViewById<TextView>(R.id.readingScoreTextView).text =
                    "Reading:\n${schoolData!!.sat_critical_reading_avg_score}"
                findViewById<TextView>(R.id.writingScoreTextView).text =
                    "Writing:\n${schoolData!!.sat_writing_avg_score}"
            }
        } else {
            Toast.makeText(this, "No Internet", Toast.LENGTH_LONG).show()
            finish()
        }
    }


    private fun getFilteredList(s: String, data: List<SchoolSAT>): Any {
        if (data.any { it.dbn == schoolID }) {
            SATData = data.filter { s in it.dbn }.first()
        } else {
            schoolData = SchoolSAT(schoolID, "N/A", "N/A", "N/A")
            return schoolData
        }
        return SATData
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}