package com.mykolak.nycschool.api

import androidx.lifecycle.MutableLiveData
import com.mykolak.nycschool.model.School
import com.mykolak.nycschool.model.SchoolSAT
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SchoolRepository {

        private val retrofit = Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/resource/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        private val api = retrofit.create(SchoolApi::class.java)

        suspend fun getSchools(): List<School> {
            return api.getSchools()
        }

        suspend fun getSchoolsSat(): List<SchoolSAT> {
            return api.getSatSchoolsScores()
        }
    }