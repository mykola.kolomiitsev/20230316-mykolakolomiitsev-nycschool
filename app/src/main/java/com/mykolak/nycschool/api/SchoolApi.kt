package com.mykolak.nycschool.api

import com.mykolak.nycschool.model.School
import com.mykolak.nycschool.model.SchoolSAT
import retrofit2.http.GET
import retrofit2.http.Query
import java.io.IOException

interface SchoolApi {
        @Throws(IOException::class)
        @GET("s3k6-pzi2.json")
        suspend fun getSchools(): List<School>
        @Throws(IOException::class)
        @GET("f9bf-2cp4.json")
        suspend fun getSatSchoolsScores(): List<SchoolSAT>
    }